FROM python:3.7-alpine

ENV PACKER_VERSION 1.4.5

RUN apk --no-cache update && \
    apk --no-cache add py3-setuptools ca-certificates curl groff less \
    openssh-client curl unzip bash git && pip --no-cache-dir install awscli && \
    curl https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip \
    -o packer.zip && unzip packer.zip && mv packer /usr/local/bin && rm packer.zip && \
    mkdir -p ~/.packer.d/plugins && \
    wget https://github.com/wata727/packer-post-processor-amazon-ami-management/releases/download/v0.7.0/packer-post-processor-amazon-ami-management_0.7.0_linux_amd64.zip -P /tmp/ && \
    cd ~/.packer.d/plugins && unzip -j /tmp/packer-post-processor-amazon-ami-management_*.zip -d ~/.packer.d/plugins && \
    rm -rf /var/cache/apk/*
