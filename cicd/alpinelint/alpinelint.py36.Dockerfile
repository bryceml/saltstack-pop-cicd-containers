FROM python:3.6-alpine3.11

RUN apk --update add wget python python-dev python3-dev enchant git gcc make libc-dev openssl-dev libffi-dev \
    curl jq zeromq-dev bash nodejs npm g++ libxml2 libxml2-dev libxslt-dev shellcheck && \
    pip3 install -U pip && pip3 install -U \
      aspy.yaml==1.1.1 \
      astroid==2.0.4 \
      atomicwrites==1.2.1 \
      attrs==18.2.0 \
      cached-property==1.5.1 \
      cfgv==1.1.0 \
      coverage==4.5.1 \
      dephell \
      identify==1.1.7 \
      importlib-metadata==0.6 \
      importlib-resources==1.0.1 \
      isort==4.3.4 \
      lazy-object-proxy==1.3.1 \
      mccabe==0.6.1 \
      modernize==0.5 \
      more-itertools==4.3.0 \
      msgpack==0.5.6 \
      nodeenv==1.3.2 \
      pathspec==0.5.9 \
      pluggy==0.7.1 \
      pre-commit==1.12.0 \
      py==1.7.0 \
      pycodestyle==2.4.0 \
      pyenchant==2.0.0 \
      pylint==2.1.1 \
      pytest-helpers-namespace==2017.11.11 \
      pytest-tempdir==2018.8.11 \
      pytest==3.10.0 \
      saltpylint==2018.10.31 \
      toml==0.10.0 \
      typed-ast \
      virtualenv==16.0.0 \
      wheel==0.32.2 \
      wrapt==1.10.11 \
      yamllint==1.12.1 \
      pyyaml==3.13 \
      six==1.11.0
